package com.planetshare.fragments.loginsignup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.planetshare.activity.R;
import com.planetshare.activity.login.EmailActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SignUpFragment extends Fragment implements View.OnClickListener {
    Unbinder unbinder;
    @BindView(R.id.cvEmail)
    CardView cvEmail;
    @BindView(R.id.cvGoogle)
    CardView cvGoogle;
    @BindView(R.id.cvFacebook)
    CardView cvFacebook;
    @BindView(R.id.cbPolicy)
    CheckBox cbPolicy;
    Context context;

    @Override
    public void onAttach(@NonNull Context mcontext) {
        super.onAttach(mcontext);
        context = mcontext;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        unbinder = ButterKnife.bind(this, view);
        setSpannableString();
        clickListner();
        return view;
    }

    private void setSpannableString() {
        String text = "I agree to Planetshare's Website Terms, Privacy Policy, and Licensing Terms.";
        int i1 = text.indexOf(",");
        int i2 = text.length();
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), i1 + 2, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        cbPolicy.setText(spannableString);
    }

    private void clickListner() {
        cvEmail.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cvGoogle:
                break;
            case R.id.cvFacebook:
                break;
            case R.id.cvEmail:
                context.startActivity(new Intent(getActivity(), EmailActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }
}
