package com.planetshare.adapters.home;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.planetshare.activity.R;
import com.planetshare.activity.imgvideodetail.ImgVideoDetailActivity;
import com.planetshare.models.home.GetImageCount;

import java.util.ArrayList;

public class ImageHorizontalAdapter extends RecyclerView.Adapter<ImageHorizontalAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<GetImageCount> imageCountList;
    private String name, catName;
    private int id;

    public ImageHorizontalAdapter(Context context, ArrayList<GetImageCount> imageCountList, String name, String catName) {
        this.context = context;
        this.imageCountList = imageCountList;
        this.name = name;
        this.catName = catName;

    }

    @NonNull
    @Override
    public ImageHorizontalAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_item_adapter, parent, false);
        return new ImageHorizontalAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHorizontalAdapter.MyViewHolder holder, int position) {
        DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
        int width_home = displaymetrics.widthPixels;
        int x, y;

        if (name.equalsIgnoreCase("celebrations")) {
            x = (int) (width_home / 2);
            y = (int) (x / 1);
            holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
            Glide.with(context)
                    .load(imageCountList.get(position).getLargeThumb())
                    /*.placeholder(R.drawable.loader)
                    .error(R.drawable.loader)*/
                    .into(holder.img_thumbnail);
        } else if (name.equalsIgnoreCase("education")) {
            x = (int) (width_home / 2);
            y = (int) (x / 1.4);
            holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
            Glide.with(context)
                    .load(imageCountList.get(position).getLargeThumb())
                    /*.placeholder(R.drawable.loader)
                    .error(R.drawable.loader)*/
                    .into(holder.img_thumbnail);
        } else if (name.equalsIgnoreCase("food & recipe") || name.equalsIgnoreCase("water fall")) {
            x = (int) (width_home / 4.2);
            y = (int) (x * 1.6);
            holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
            Glide.with(context)
                    .load(imageCountList.get(position).getLargeThumb())
                    /*.placeholder(R.drawable.loader)
                    .error(R.drawable.loader)*/
                    .into(holder.img_thumbnail);
        } else {
            x = (int) (width_home / 2.3);
            y = (int) (x / 1);
            holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
            Glide.with(context)
                    .load(imageCountList.get(position).getLargeThumb())
                    /*.placeholder(R.drawable.loader)
                    .error(R.drawable.loader)*/
                    .into(holder.img_thumbnail);

        }

        holder.cvParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImgVideoDetailActivity.class);
                intent.putExtra("catName", catName);
                intent.putExtra("thumbUrl", imageCountList.get(holder.getAdapterPosition()).getLargeThumb());
                intent.putExtra("titleName", imageCountList.get(holder.getAdapterPosition()).getTitle());
                intent.putExtra("imageCatId", String.valueOf(imageCountList.get(holder.getAdapterPosition()).getImageCategoryId()));
                intent.putExtra("catId", String.valueOf(imageCountList.get(holder.getAdapterPosition()).getId()));

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return imageCountList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_thumbnail;
        private TextView tvName, tvCount;
        private CardView cvParent;
        private LinearLayout llBtmName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_thumbnail = itemView.findViewById(R.id.img_thumb_nail);
            llBtmName = itemView.findViewById(R.id.llbtmname);
            tvName = itemView.findViewById(R.id.tvName);
            tvCount = itemView.findViewById(R.id.tvCount);
            cvParent = itemView.findViewById(R.id.cvParent);
            tvCount.setVisibility(View.GONE);
            tvName.setVisibility(View.GONE);
            llBtmName.setVisibility(View.GONE);
        }
    }
}
