package com.planetshare.adapters.home;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.planetshare.activity.R;
import com.planetshare.models.home.GetVideoCount;

import java.util.ArrayList;

public class VideoHorizontalAdapter extends RecyclerView.Adapter<VideoHorizontalAdapter.MyViewHolder> {
   private Context context;
   private ArrayList<GetVideoCount> videoCountList;

    public VideoHorizontalAdapter(Context context, ArrayList<GetVideoCount> videoCountList) {
        this.context = context;
        this.videoCountList = videoCountList;
    }

    @NonNull
    @Override
    public VideoHorizontalAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_item_adapter, parent, false);
        return new VideoHorizontalAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHorizontalAdapter.MyViewHolder holder, int position) {
        DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
        int width_home = displaymetrics.widthPixels;
        int x, y;
        x = (int) (width_home / 2.3);
        y = (int) (x / 1);
        holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
        Glide.with(context)
                .load(videoCountList.get(position).getLargeThumb())
                /*.placeholder(R.drawable.loader)
                .error(R.drawable.loader)*/
                .into(holder.img_thumbnail);
    }

    @Override
    public int getItemCount() {
        return videoCountList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_thumbnail;
        private TextView tvName, tvCount;
        private CardView cvParent;
        private LinearLayout llBtmName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_thumbnail = itemView.findViewById(R.id.img_thumb_nail);
            llBtmName = itemView.findViewById(R.id.llbtmname);
            tvName = itemView.findViewById(R.id.tvName);
            tvCount = itemView.findViewById(R.id.tvCount);
            cvParent = itemView.findViewById(R.id.cvParent);
            tvCount.setVisibility(View.GONE);
            tvName.setVisibility(View.GONE);
            llBtmName.setVisibility(View.GONE);
        }
    }
}
