package com.planetshare.adapters.home;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.planetshare.activity.R;

public class SliderAdapter extends PagerAdapter {
    private Context context;
    private int[] sliderImageId = new int[]{
            R.drawable.slider1, R.drawable.slider2, R.drawable.slider3, R.drawable.slider4, R.drawable.slider5,
    };
    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return sliderImageId.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(sliderImageId[position]);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
       /* View itemView = LayoutInflater.from(context).inflate(R.layout.slider_item,
                container, false);
*/
//        ivSliderImage.setImageResource(sliderImageId[position]);
//        ((ViewPager) container).addView(ivThumbNail, 0);
        /* TextView tvNameSlider = (TextView) itemView.findViewById(R.id.tvNameSlider);
        ImageView ivSliderImage = (ImageView) itemView.findViewById(R.id.ivSliderImage);
         *//*       Glide.with(mContext)
                .load(mResources.get(position).getLargeImgurl())
                   .into(ivSliderImage);*//*
        container.addView(itemView);*/
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeView((FrameLayout) object);
        ((ViewPager) container).removeView((ImageView) object);
    }
}
