package com.planetshare.adapters.imgvideodetail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.models.collection.getusercollection.UserData;

import java.util.ArrayList;

public class UserCollectionListAdpater extends RecyclerView.Adapter<UserCollectionListAdpater.MyViewHolder> {
    private ArrayList<UserData> userCollectionListData;
    Context context;

    public UserCollectionListAdpater(Context context, ArrayList<UserData> userCollectionListData) {
        this.context = context;
        this.userCollectionListData = userCollectionListData;
    }

    @NonNull
    @Override
    public UserCollectionListAdpater.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tags_item, parent, false);
        return new UserCollectionListAdpater.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserCollectionListAdpater.MyViewHolder holder, int position) {
        holder.tvTags.setText(userCollectionListData.get(position).getCollectionName());
    }

    @Override
    public int getItemCount() {
        return userCollectionListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTags;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTags = itemView.findViewById(R.id.tvTags);
        }
    }
}
