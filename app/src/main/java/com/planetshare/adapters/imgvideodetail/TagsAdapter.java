package com.planetshare.adapters.imgvideodetail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;

import java.util.List;

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.MyViewHolder> {
    Context context;
    private List<String> keyWordList;

    public TagsAdapter(Context context, List<String> imgVidData) {
        this.context = context;
        this.keyWordList = imgVidData;
    }

    @NonNull
    @Override
    public TagsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tags_item, parent, false);
        return new TagsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TagsAdapter.MyViewHolder holder, int position) {
        holder.tvTags.setText(keyWordList.get(position));
    }

    @Override
    public int getItemCount() {
        return keyWordList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTags;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTags = itemView.findViewById(R.id.tvTags);
        }
    }
}
