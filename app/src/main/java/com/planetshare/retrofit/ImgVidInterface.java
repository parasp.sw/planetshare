package com.planetshare.retrofit;

import com.planetshare.models.imagevideodetails.ImgVidResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ImgVidInterface {
    @FormUrlEncoded
    @POST("image_details")
    Call<ImgVidResponse> getImgVideoResponse(
            @Field("user_id") int user_id,
            @Field("cat_name") String cat_name,
            @Field("image_category_id") String image_category_id,
            @Field("id") String id,
            @Field("tag") String tag
    );
}
