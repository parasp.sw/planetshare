package com.planetshare.retrofit;

import com.planetshare.models.viewall.ViewAllRespone;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ViewAllApiInterface {
    @FormUrlEncoded
    @POST("view_all")
    Call<ViewAllRespone> getViewAllResponse(
            @Field("id") String id,
            @Field("cat_name") String cat_name,
            @Field("user_id") int user_id,
            @Field("tag") String tag
    );
}
