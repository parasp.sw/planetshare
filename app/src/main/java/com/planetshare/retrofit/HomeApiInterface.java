package com.planetshare.retrofit;

import com.planetshare.models.home.HomeResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface HomeApiInterface {
    @GET("home_page")
    Call<HomeResponse> getHomeData();
}
