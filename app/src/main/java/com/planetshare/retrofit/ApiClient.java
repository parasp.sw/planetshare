package com.planetshare.retrofit;

import android.content.Context;
import android.util.Base64;

import com.planetshare.localstorage.SessionManager;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Urls;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder().readTimeout(180, TimeUnit.SECONDS)
            .connectTimeout(180, TimeUnit.SECONDS);
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Urls.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder builder2 =
            new Retrofit.Builder()
                    .baseUrl(Urls.BASE_URL_NEW)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass, Context context) {
        SessionManager sessionManager = new SessionManager(context);
        if (Constants.UserNameAuth != null && Constants.PasswordAuth != null) {
            String credentials = Constants.UserNameAuth + ":" + Constants.PasswordAuth;
            final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response mainResponse = chain.proceed(chain.request());
                    Request mainRequest = chain.request();
                    if (mainResponse.code() == 401 || mainResponse.code() == 403) {
                        Request.Builder builder = mainRequest.newBuilder().header("Authorization", basic).
                                header("Accept", "application/json").
                                header("Authorization", sessionManager.getToken()).
                                method(mainRequest.method(), mainRequest.body());
                        mainResponse = chain.proceed(builder.build());
                    }
                    return mainResponse;


                }
            });
        }
        Retrofit retrofit = null;
        OkHttpClient client = httpClient.build();
        retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createServiceNew(Class<S> serviceClass, Context context) {
        SessionManager sessionManager = new SessionManager(context);
        if (Constants.UserNameAuth != null && Constants.PasswordAuth != null) {
            String credentials = Constants.UserNameAuth + ":" + Constants.PasswordAuth;
            final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response mainResponse = chain.proceed(chain.request());
                    Request mainRequest = chain.request();
                    if (mainResponse.code() == 401 || mainResponse.code() == 403) {
                        Request.Builder builder = mainRequest.newBuilder().header("Authorization", basic).
                                header("Accept", "application/json").
                                header("Authorization", sessionManager.getToken()).
                                method(mainRequest.method(), mainRequest.body());
                        mainResponse = chain.proceed(builder.build());
                    }
                    return mainResponse;
                }
            });
        }
        Retrofit retrofit = null;
        OkHttpClient client = httpClient.build();
        retrofit = builder2.client(client).build();
        return retrofit.create(serviceClass);
    }
}
