package com.planetshare.retrofit;

import com.planetshare.models.collection.createcollection.CreateCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserCollectionResponse;
import com.planetshare.models.collection.insertcollection.InsertCollectionResposne;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CollectionApiInterface {
    /*CreateApi*/
    @FormUrlEncoded
    @POST("create/collection")
    Call<CreateCollectionResponse> getCreateCollectionResponse(
            @Field("user_id") int user_id,
            @Field("collection_type") String collection_type,
            @Field("collection_name") String collection_name,
            @Header("Authorization") String authKey,
            @Field("tag") String tag
    );

    /*InsertApi*/
    @FormUrlEncoded
    @POST("insert/collection")
    Call<InsertCollectionResposne> getInsertResponse(
            @Field("user_id") int user_id,
            @Field("item_id") String item_id,
            @Field("item_type") String item_type,
            @Field("collection_id") int collection_id,
            @Header("Authorization") String authKey,
            @Field("tag") String tag
    );

    /*UserList*/
    @FormUrlEncoded
    @POST("getUserCollection")
    Call<UserCollectionResponse> getUserCollectionResponse(
            @Field("user_id") int user_id,
            @Field("collection_type") String collection_type,
            @Header("Authorization") String authKey,
            @Field("tag") String tag
    );
}
