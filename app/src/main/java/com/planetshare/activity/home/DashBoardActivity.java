package com.planetshare.activity.home;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.planetshare.activity.R;
import com.planetshare.adapters.home.SliderAdapter;
import com.planetshare.adapters.viewpagerAdpater.ViewPagerAdapter;
import com.planetshare.fragments.home.ImageFragment;
import com.planetshare.fragments.home.VideoFragment;
import com.planetshare.models.home.HomeResponse;
import com.planetshare.models.home.ImageCat;
import com.planetshare.models.home.VideoCat;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.HomeApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.CustomViewPager;
import com.planetshare.utils.Utils;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.viewPagerSlider)
    ViewPager viewPagerSlider;
    @BindView(R.id.viewPager)
    CustomViewPager viewPager;
    @BindView(R.id.circlePageIndicator)
    CirclePageIndicator circlePageIndicator;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.llBtmHome)
    LinearLayout llBtmHome;
    @BindView(R.id.llBtmSearch)
    LinearLayout llBtmSearch;
    @BindView(R.id.llBtmCollection)
    LinearLayout llBtmCollection;
    @BindView(R.id.llBtmProfile)
    LinearLayout llBtmProfile;
    @BindView(R.id.pbHome)
    ProgressBar pbHome;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    private boolean isNetworkConnected;
    private ArrayList<ImageCat> imageListing;
    private ArrayList<VideoCat> videoListing;
    private static int NUM_PAGES = 0;
    private static int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        initailizeView();
        setSlider();
        if (Utils.checkInternetConnection(this)) {
            getHomeData();
        } else {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
    }

    private void setSlider() {
        viewPagerSlider.setAdapter(new SliderAdapter(this));
        circlePageIndicator.setViewPager(viewPagerSlider);
        final float density = getResources().getDisplayMetrics().density;
        circlePageIndicator.setViewPager(viewPagerSlider);
        circlePageIndicator.setRadius(3 * density);
//        NUM_PAGES = carouselList.size();
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPagerSlider.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 10000, 10000);

        // Pager listener over circlePageIndicator
        circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });
    }

    private void initailizeView() {
        imageListing = new ArrayList<>();
        videoListing = new ArrayList<>();
        isNetworkConnected = Utils.checkInternetConnection(this);
        if (!isNetworkConnected) {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
        clickListner();
    }

    private void clickListner() {
        llBtmHome.setOnClickListener(this);
        llBtmSearch.setOnClickListener(this);
        llBtmCollection.setOnClickListener(this);
        llBtmProfile.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        if (Utils.checkInternetConnection(this)) {
//            getHomeData();
        } else {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
        super.onResume();
    }

    private void getHomeData() {
        pbHome.setVisibility(View.VISIBLE);
        HomeApiInterface homeApiInterface = ApiClient.createService(HomeApiInterface.class,this);
        Call<HomeResponse> homeResponse = homeApiInterface.getHomeData();
        homeResponse.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                pbHome.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        tvNoData.setVisibility(View.GONE);
                        imageListing.clear();
                        videoListing.clear();
                        if (response.body().getImageCat() != null && response.body().getImageCat().size() > 0) {
                            imageListing.addAll(response.body().getImageCat());
                        }
                        if (response.body().getVideoCat() != null && response.body().getVideoCat().size() > 0) {
                            videoListing.addAll(response.body().getVideoCat());
                        }
                        fireFragments();
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                pbHome.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(DashBoardActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(DashBoardActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }

            }
        });
    }

    private void fireFragments() {
        if (Utils.checkInternetConnection(getApplicationContext())) {
            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragment((ImageFragment.newInstance(DashBoardActivity.this, imageListing)), "Photos");
            viewPagerAdapter.addFragment((VideoFragment.newInstance(DashBoardActivity.this, videoListing)), "Videos");
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.setPagingEnabled(false);
        } else {
            Toast.makeText(DashBoardActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBtmHome:
                break;
            case R.id.llBtmSearch:
                break;
            case R.id.llBtmCollection:
                break;
            case R.id.llBtmProfile:
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        moveTaskToBack(true);
    }
}
