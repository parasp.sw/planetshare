package com.planetshare.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.planetshare.activity.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeLoginSignUp extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnSignup)
    Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_login_sign_up);
        ButterKnife.bind(this);
        clickListner();
    }

    private void clickListner() {
        btnLogin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.btnSignup:
                startActivity(new Intent(this, SignUpActivity.class));
                break;
            default:
                break;
        }
    }
}
