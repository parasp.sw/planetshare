package com.planetshare.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.login.LoginResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.LoginApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    /*activity_login*/
    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;
    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;
    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.ivFacebook)
    ImageView ivFacebook;
    @BindView(R.id.ivGoogle)
    ImageView ivGoogle;
    @BindView(R.id.pBar)
    ProgressBar pBar;
    @BindView(R.id.tvSignUp)
    TextView tvSignUp;
    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;
    SessionManager sessionManager;
    private String authProvider, userName, socialEmail, authProviderId, tag = "mobile";
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(LoginActivity.this);
        setSpannableString();
        FirebaseApp.initializeApp(LoginActivity.this);
        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                .enableAutoManage(LoginActivity.this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        clickListner();
    }

    private void setSpannableString() {
        String text = "Don't have an account? Sign Up";
        int i1 = text.indexOf("?");
        int i2 = text.length();
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), i1 + 2, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvSignUp.setText(spannableString);
    }

    private void clickListner() {
        btnLogin.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivGoogle.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivFacebook:
                if (Utils.checkInternetConnection(LoginActivity.this)) {
                    facebookLogin();
                } else {
                    Toast.makeText(LoginActivity.this, "Internet Connection not available!", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.ivGoogle:
                if (Utils.checkInternetConnection(LoginActivity.this)) {
                    googleSignIn();
                } else {
                    Toast.makeText(LoginActivity.this, "Internet Connection not available!", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnLogin:
//                startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                if (Utils.checkInternetConnection(LoginActivity.this)) {
                    /*   email*/
                    if (edtEmail.getText().toString().trim().isEmpty()) {
                        tilEmail.setError("Please enter email");
                        return;
                    }
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                    if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches()) {
                        tilEmail.setError("Please enter valid email");
                        return;
                    }
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                    /*password*/
                    if (edtPassword.getText().toString().trim().isEmpty()) {
                        tilPassword.setError("Please enter password");
                        return;
                    }
                    tilPassword.setError(null);
                    tilPassword.setErrorEnabled(false);
                    getLogin(edtEmail.getText().toString().trim(), edtPassword.getText().toString().trim());

                } else {
                    Toast.makeText(LoginActivity.this, "Internet Connection not available!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvForgotPassword:
                startActivity(new Intent(this, ForgotPasswordActivity.class));

                break;
            case R.id.tvSignUp:
                startActivity(new Intent(this, SignUpActivity.class));
                break;
            default:
                break;
        }
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                authProvider = "facebook";
                authProviderId = loginResult.getAccessToken().getUserId();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    String id = object.getString("id");
                                    String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";
                                    String last_name = object.getString("last_name");
                                    String gender = object.getString("gender");
                                    String birthday = object.getString("birthday");
                                    //String email = response.getJSONObject().getString("email");
                                    if (object.getString("first_name") != null)
                                        userName = object.getString("first_name");
                                    if (object.getString("email") != null)
                                        socialEmail = object.getString("email");
                                    else
                                        socialEmail = "";
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                getFacebookResponse();
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,first_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Facebook Login cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Facebook Login Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            authProvider = "Google";
            authProviderId = acct.getId();
            String personName = acct.getDisplayName();
            if (acct.getPhotoUrl() != null) {
                String googleImageUrl = acct.getPhotoUrl().toString();
            }
            userName = acct.getDisplayName();
            socialEmail = acct.getEmail();
        } else {
            Toast.makeText(LoginActivity.this, "Could not sign in via google", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFacebookResponse() {
    }

    private void getLogin(String edtEmail, String edtPassword) {
        pBar.setVisibility(View.VISIBLE);
        LoginApiInterface loginApiInterface = ApiClient.createServiceNew(LoginApiInterface.class,this);
        Call<LoginResponse> loginResponse = loginApiInterface.getLoginResponse(edtEmail, edtPassword, tag);
        loginResponse.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                pBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        String msg = response.body().getMsg();
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    } else if (response.body().getSuccess() == 1) {
                        sessionManager.saveToken("Bearer " + response.body().getToken().trim());
                        sessionManager.saveAccountId(response.body().getAccountId());
                        sessionManager.saveUserName(response.body().getName());
                        sessionManager.saveEmailId(response.body().getEmail());
                        sessionManager.saveUserId((response.body().getUserId()), true);
                        sessionManager.saveVendorId(response.body().getVendorId());
                        sessionManager.saveSellerId(response.body().getSellerId());
                        Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        LoginActivity.this.startActivity(intent);
                        Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
}
