package com.planetshare.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.login.LoginResponse;
import com.planetshare.models.signupemail.SignupResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.LoginApiInterface;
import com.planetshare.retrofit.SignUpApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tilName)
    TextInputLayout tilName;
    @BindView(R.id.edtName)
    TextInputEditText edtName;

    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;

    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;
    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;

    @BindView(R.id.tilCnfPassword)
    TextInputLayout tilCnfPassword;
    @BindView(R.id.edtConfirmPassword)
    TextInputEditText edtConfirmPassword;
    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.pBar)
    ProgressBar pBar;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        ButterKnife.bind(this);
        intializeView();
        clickListner();
    }

    private void intializeView() {
        sessionManager = new SessionManager(this);
    }

    private void clickListner() {
        btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                if (Utils.checkInternetConnection(EmailActivity.this)) {
                    /*username*/
                    if (edtName.getText().toString().trim().isEmpty()) {
                        tilName.setError("Please enter user name");
                        return;
                    }
                    tilName.setError(null);
                    tilName.setErrorEnabled(false);
                    /*email*/
                    if (edtEmail.getText().toString().trim().isEmpty()) {
                        tilEmail.setError("Please enter email");
                        return;
                    }
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                    if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches()) {
                        tilEmail.setError("Please enter valid email");
                        return;
                    }
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                    /*password*/
                    if (edtPassword.getText().toString().trim().isEmpty()) {
                        tilPassword.setError("Please enter password");
                        return;
                    }
                    tilPassword.setError(null);
                    tilPassword.setErrorEnabled(false);
                    if (!edtPassword.getText().toString().trim().equals(edtConfirmPassword.getText().toString().trim())) {
                        tilCnfPassword.setError("Password mismatch");
                        return;
                    }
                    tilCnfPassword.setError(null);
                    tilCnfPassword.setErrorEnabled(false);

                    getSignUp(edtName.getText().toString().trim(),
                            edtEmail.getText().toString().trim(), edtPassword.getText().toString().trim());

                } else {
                    Toast.makeText(EmailActivity.this, "Internet Connection not available!", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private void getSignUp(String edtUserName, String edtEmail, String edtPassword) {
        pBar.setVisibility(View.VISIBLE);
        SignUpApiInterface signUpApiInterface = ApiClient.createServiceNew(SignUpApiInterface.class,this);
        Call<SignupResponse> signupResponse = signUpApiInterface.getSignUpResponse(edtUserName, edtEmail, edtPassword, "mobile");
        signupResponse.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                pBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        String fail = response.body().getFail();
                        Toast.makeText(EmailActivity.this, fail, Toast.LENGTH_SHORT).show();
                    } else if (response.body().getSuccess() == 1) {
                        getLogin(edtEmail, edtPassword);
                        String msg = response.body().getMsg();
                        Toast.makeText(EmailActivity.this, msg, Toast.LENGTH_SHORT).show();
                    } else if (response.body().getSuccess() == 2) {
                        String msg = response.body().getMsg();
                        Toast.makeText(EmailActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 3) {
                        String error = response.body().getError();
                        Toast.makeText(EmailActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(EmailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                pBar.setVisibility(View.GONE);
                Toast.makeText(EmailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getLogin(String edtEmail, String edtPassword) {
        pBar.setVisibility(View.VISIBLE);
        LoginApiInterface loginApiInterface = ApiClient.createServiceNew(LoginApiInterface.class,this);
        Call<LoginResponse> loginResponse = loginApiInterface.getLoginResponse(edtEmail, edtPassword, "mobile");
        loginResponse.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                pBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        String msg = response.body().getMsg();
                        Toast.makeText(EmailActivity.this, msg, Toast.LENGTH_SHORT).show();
                    } else if (response.body().getSuccess() == 1) {
                        String msg = response.body().getMsg();
                        Toast.makeText(EmailActivity.this, msg, Toast.LENGTH_SHORT).show();
                        /*SaveDataInSharedPrefrence*/
                        sessionManager.saveToken(response.body().getToken());
                        sessionManager.saveAccountId(response.body().getAccountId());
                        sessionManager.saveUserName(response.body().getName());
                        sessionManager.saveEmailId(response.body().getEmail());
                        sessionManager.saveUserId((response.body().getUserId()), true);
                        sessionManager.saveVendorId(response.body().getVendorId());
                        sessionManager.saveSellerId(response.body().getSellerId());
                        Intent intent = new Intent(EmailActivity.this, DashBoardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EmailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                }
            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pBar.setVisibility(View.GONE);
                Toast.makeText(EmailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
