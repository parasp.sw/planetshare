package com.planetshare.activity.splash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.planetshare.activity.R;
import com.planetshare.activity.welcome.WelcomeActivity;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.vidSplash)
    VideoView videoView;
    @BindView(R.id.txtversion)
    TextView txtversion;
    SessionManager session;
    boolean isNetworkConnected;
    SharedPreferences sharedpreferences;
    private String Sha1Key = "";
    private String versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sharedpreferences = getSharedPreferences("Store", Context.MODE_PRIVATE);
        session = new SessionManager(getBaseContext());
        getVersionInfo();
        /*splash_video_set*/
        Uri path = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.planetsharenew);
        videoView.setVideoURI(path);
        videoView.start();
        jump();
        //   hashFromSHA1(Sha1Key);

    }

    private void jump() {
        txtversion.setText("Version : " + versionName);
        isNetworkConnected = Utils.checkInternetConnection(SplashActivity.this);
        if (isNetworkConnected) {
            if (Build.VERSION.SDK_INT > 21) {
                new Handler().postDelayed(new Runnable() {


                                              public void run() {
                                                  if (session.isFirstTimeLaunch()) {
                                                      session.checkLogin();
                                                  } else {
                                                      startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));

                                                  }
                                                  finish();
                                              }
                                          }
                        , 4000);
            }
        } else {
            Utils.restartDialogue(SplashActivity.this, "No Internet Connection Available", true);
        }

    }

    private void getVersionInfo() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void hashFromSHA1(String sha1) {
        String[] arr = sha1.split(":");
        byte[] byteArr = new byte[arr.length];

        for (int i = 0; i < arr.length; i++) {
            byteArr[i] = Integer.decode("0x" + arr[i]).byteValue();
        }

        Log.e("hash : ", Base64.encodeToString(byteArr, Base64.NO_WRAP));
    }
}