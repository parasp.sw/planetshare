package com.planetshare.activity.imgvideodetail;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.adapters.imgvideodetail.ImgVidAdapter;
import com.planetshare.adapters.imgvideodetail.TagsAdapter;
import com.planetshare.adapters.imgvideodetail.UserCollectionListAdpater;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.collection.createcollection.CreateCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserData;
import com.planetshare.models.collection.insertcollection.InsertCollectionResposne;
import com.planetshare.models.imagevideodetails.ImgVidData;
import com.planetshare.models.imagevideodetails.ImgVidResponse;
import com.planetshare.models.imagevideodetails.SimilarData;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.CollectionApiInterface;
import com.planetshare.retrofit.ImgVidInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImgVideoDetailActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.rvImgVidDetails)
    RecyclerView rvImgVidDetails;
    @BindView(R.id.rvTags)
    RecyclerView rvTags;
    @BindView(R.id.pbarImgVidDetails)
    ProgressBar pbarImgDetails;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.ivThumb)
    ImageView ivThumb;
    @BindView(R.id.ivDownload)
    ImageView ivDownload;
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @BindView(R.id.ivImgInfo)
    ImageView ivImgInfo;
    @BindView(R.id.ivCross)
    ImageView ivCross;
    @BindView(R.id.ivAddToCollection)
    ImageView ivAddToCollection;
    @BindView(R.id.tvName)
    TextView tvName;
    String titleName, imageCatId, catId, catName, thumbUrl, keywords, itemId, collectionName;
    private int userId = 0, collectionId;
    private ArrayList<ImgVidData> imgVidData;
    private ArrayList<SimilarData> similarData;
    SessionManager sessionManager;
    private boolean isNetworkConnected;
    private String token;
    private ArrayList<UserData> userCollectionListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_video_detail);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        token = sessionManager.getToken();
        userId = sessionManager.getUserId();
        Log.d("Token", token);
        Log.d("UserId", String.valueOf(userId));
        if (getIntent().getStringExtra("thumbUrl") != null) {
            thumbUrl = getIntent().getStringExtra("thumbUrl");
        }
        if (getIntent().getStringExtra("catId") != null) {
            catId = getIntent().getStringExtra("catId");
            itemId = catId;
        }
        if (getIntent().getStringExtra("imageCatId") != null) {
            imageCatId = getIntent().getStringExtra("imageCatId");
        }
        titleName = getIntent().getStringExtra("titleName");
        tvName.setText(titleName);
        catName = getIntent().getStringExtra("catName");

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        initializeView();
        isNetworkConnected = Utils.checkInternetConnection(this);
        if (!isNetworkConnected) {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
        getImgVidDetails(catName, catId, imageCatId);
        titleName = getIntent().getStringExtra("name");
        collapsingToolbar.setTitle(titleName);

    }


    private void initializeView() {
        userCollectionListData = new ArrayList<>();
        imgVidData = new ArrayList<>();
        similarData = new ArrayList<>();
        Glide.with(getApplicationContext())
                .load(thumbUrl)
                .into(ivThumb);
        rvImgVidDetails.setHasFixedSize(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            }
        });
        clickListner();
    }

    private void clickListner() {
        ivCross.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivAddToCollection.setOnClickListener(this);
    }

    private void getImgVidDetails(String catName, String catId, String imageCatId) {
        pbarImgDetails.setVisibility(View.VISIBLE);
        ImgVidInterface imgVidInterface = ApiClient.createService(ImgVidInterface.class, this);
        Call<ImgVidResponse> imgVidResponse = imgVidInterface.getImgVideoResponse(userId, catName, imageCatId, catId, Constants.TAG);
        imgVidResponse.enqueue(new Callback<ImgVidResponse>() {
            @Override
            public void onResponse(Call<ImgVidResponse> call, Response<ImgVidResponse> response) {
                pbarImgDetails.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        similarData.clear();
                        imgVidData.clear();
                        if (response.body().getSimilarData() != null && response.body().getSimilarData().size() > 0) {
                            similarData.addAll(response.body().getSimilarData());
                            rvImgVidDetails.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                            ImgVidAdapter imgVidAdapter = new ImgVidAdapter(ImgVideoDetailActivity.this, similarData, ImgVideoDetailActivity.this.catName);
                            rvImgVidDetails.setAdapter(imgVidAdapter);
                            /* SpacesItemDecoration decoration = new SpacesItemDecoration(40);
                            rvImgVidDetails.addItemDecoration(decoration);*/
                        }
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            imgVidData.addAll(response.body().getData());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ImgVideoDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvTags.setLayoutManager(linearLayoutManager);
                            for (int i = 0; i < imgVidData.size(); i++) {
                                List<String> keyWordList = Arrays.asList(imgVidData.get(i).getKeywords().split(","));
                                TagsAdapter tagsAdapter = new TagsAdapter(ImgVideoDetailActivity.this, keyWordList);
                                rvTags.setAdapter(tagsAdapter);
                            }
                        }
                    } else {
                        Toast.makeText(ImgVideoDetailActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ImgVidResponse> call, Throwable t) {
                Toast.makeText(ImgVideoDetailActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                pbarImgDetails.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCross:
                onBackPressed();
                break;

            case R.id.ivAddToCollection:
                addToCollection();
                break;

            case R.id.ivShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                break;

            default:
                break;
        }
    }

    private void addToCollection() {
        final Dialog dialog = new Dialog(this, R.style.FullScreenDialogStyle);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
        dialog.setContentView(R.layout.collection_dialog_layout);
        EditText edtCollection = dialog.findViewById(R.id.edtCollection);
        ProgressBar pbarCollection = dialog.findViewById(R.id.pbarCollection);
        RecyclerView rvUsersList = dialog.findViewById(R.id.rvUsersList);
        Button btnSaveCollection = dialog.findViewById(R.id.btnSaveCollection);
        /*UserListing With Name*/
        getUserList(rvUsersList, pbarCollection);
        /*Create a UserCollectionWith Name*/
        btnSaveCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCollection(edtCollection.getText().toString().trim(), pbarCollection);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /*Create a Collection */
    private void createCollection(String edtCollection, ProgressBar pbarCollection) {
        pbarCollection.setVisibility(View.VISIBLE);
        collectionName = edtCollection;
        CollectionApiInterface createCollectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<CreateCollectionResponse> createCollectionResponse = createCollectionApiInterface.getCreateCollectionResponse(userId, catName, collectionName, token, Constants.TAG);
        createCollectionResponse.enqueue(new Callback<CreateCollectionResponse>() {
            @Override
            public void onResponse(Call<CreateCollectionResponse> call, Response<CreateCollectionResponse> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        Toast.makeText(ImgVideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 1) {
                        collectionId = response.body().getCollectionId();
                        insertItem(pbarCollection, collectionId);
                    }
                    if (response.body().getSuccess() == 2) {
                        Toast.makeText(ImgVideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ImgVideoDetailActivity.this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateCollectionResponse> call, Throwable t) {
                Toast.makeText(ImgVideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                pbarCollection.setVisibility(View.GONE);
            }
        });
    }

    /*UserList ShowIn in Dialog*/
    private void getUserList(RecyclerView rvUsersList, ProgressBar pbarCollection) {
        pbarCollection.setVisibility(View.VISIBLE);
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<UserCollectionResponse> userCollectionResponse = collectionApiInterface.getUserCollectionResponse(userId, catName, token, Constants.TAG);
        userCollectionResponse.enqueue(new Callback<UserCollectionResponse>() {
            @Override
            public void onResponse(Call<UserCollectionResponse> call, Response<UserCollectionResponse> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        userCollectionListData.clear();
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            userCollectionListData.addAll(response.body().getData());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ImgVideoDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvUsersList.setLayoutManager(linearLayoutManager);
                            UserCollectionListAdpater userCollectionListAdpater = new UserCollectionListAdpater(ImgVideoDetailActivity.this, userCollectionListData);
                            rvUsersList.setAdapter(userCollectionListAdpater);
                            userCollectionListAdpater.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(ImgVideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCollectionResponse> call, Throwable t) {
                Toast.makeText(ImgVideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                pbarCollection.setVisibility(View.GONE);
            }
        });
    }

    /*InsertItemInCollection*/
    private void insertItem(ProgressBar pbarCollection, int collectionId) {
        pbarCollection.setVisibility(View.VISIBLE);
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<InsertCollectionResposne> insertCollectionResposne = collectionApiInterface.getInsertResponse(userId, itemId, catName, collectionId, token, Constants.TAG);
        insertCollectionResposne.enqueue(new Callback<InsertCollectionResposne>() {
            @Override
            public void onResponse(Call<InsertCollectionResposne> call, Response<InsertCollectionResposne> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        Toast.makeText(ImgVideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(ImgVideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 2) {
                        Toast.makeText(ImgVideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ImgVideoDetailActivity.this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InsertCollectionResposne> call, Throwable t) {
                Toast.makeText(ImgVideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                pbarCollection.setVisibility(View.GONE);
            }
        });

    }

    /*Related CallBack for Reloading Data*/
    public void relatedCallBackFromAdpater(String catName, String imageCategoryId, String catId, String thumbNail) {
        getImgVidDetails(catName, catId, imageCategoryId);
        Glide.with(getApplicationContext())
                .load(thumbNail)
                .into(ivThumb);
        itemId = catId;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashBoardActivity.class));
        overridePendingTransition(
                0,
                R.anim.animbackpress
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

