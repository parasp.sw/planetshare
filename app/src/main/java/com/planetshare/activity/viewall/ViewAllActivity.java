package com.planetshare.activity.viewall;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.planetshare.activity.R;
import com.planetshare.adapters.viewall.ViewAllAdapter;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.viewall.ViewAllListing;
import com.planetshare.models.viewall.ViewAllRespone;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.ViewAllApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.SpacesItemDecoration;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.rvViewALl)
    RecyclerView rvViewALl;
    @BindView(R.id.pbarViewAll)
    ProgressBar pbarViewAll;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    private String catName, catId, name;
    private int userId = 0;
    SessionManager sessionManager;
    private ArrayList<ViewAllListing> viewAllListing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        if (getIntent().getStringExtra("catId") != null) {
            catId = getIntent().getStringExtra("catId");
        }
        name = getIntent().getStringExtra("name");
        tvName.setText(name);
        catName = getIntent().getStringExtra("catName");
        userId = sessionManager.getUserId();
        initializeView();
        getViewAllData();
    }

    private void initializeView() {
        viewAllListing = new ArrayList<>();
        tvName.setText(name);
        ivBack.setOnClickListener(this);

    }

    private void getViewAllData() {
        pbarViewAll.setVisibility(View.VISIBLE);
        ViewAllApiInterface viewAllApiInterface = ApiClient.createService(ViewAllApiInterface.class,this);
        Call<ViewAllRespone> viewAllRespone = viewAllApiInterface.getViewAllResponse(catId, catName, userId, Constants.TAG);
        viewAllRespone.enqueue(new Callback<ViewAllRespone>() {
            @Override
            public void onResponse(Call<ViewAllRespone> call, Response<ViewAllRespone> response) {
                pbarViewAll.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        tvNoData.setVisibility(View.GONE);
                        if (response.body().getData() != null && response.body().getData().getViewAllListing() != null && response.body().getData().getViewAllListing().size() > 0) {

                            viewAllListing.addAll(response.body().getData().getViewAllListing());
                            rvViewALl.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                            ViewAllAdapter viewAllAdapter = new ViewAllAdapter(ViewAllActivity.this, viewAllListing);
                            rvViewALl.setAdapter(viewAllAdapter);
                            SpacesItemDecoration decoration = new SpacesItemDecoration(16);
                            rvViewALl.addItemDecoration(decoration);

                        }

                    } else {
                        tvNoData.setVisibility(View.VISIBLE);

                    }
                }
            }

            @Override
            public void onFailure(Call<ViewAllRespone> call, Throwable t) {
                pbarViewAll.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(ViewAllActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ViewAllActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
 /*viewAllListing.addAll(response.body().getData().getViewAllListing());
                            SpannableGridLayoutManager gridLayoutManager = new
                                    SpannableGridLayoutManager(new SpannableGridLayoutManager.GridSpanLookup() {
                                @Override
                                public SpannableGridLayoutManager.SpanInfo getSpanInfo(int position) {
                                    if (position % 2 == 0) {
                                        return new SpannableGridLayoutManager.SpanInfo(1, 2);
                                        //this will count of row and column you want to replace
                                    } else {
                                        return new SpannableGridLayoutManager.SpanInfo(3, 2);
                                    }
                                }
                            }, 3, 1f); // 3 is the number of coloumn , how nay to display is 1f
//                            rvViewALl.setLayoutManager(gridLayoutManager);

                            /* LinearLayoutManager horizontalLayoutmanager1 = new LinearLayoutManager(ViewAllActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvViewALl.setLayoutManager(horizontalLayoutmanager1);*/
/*StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
                            rvViewALl.setLayoutManager(staggeredGridLayoutManager);
                                    ViewAllAdapter viewAllAdapter = new ViewAllAdapter(ViewAllActivity.this, viewAllListing);
                                    rvViewALl.setAdapter(viewAllAdapter);


                                    */