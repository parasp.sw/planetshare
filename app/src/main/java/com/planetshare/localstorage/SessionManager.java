package com.planetshare.localstorage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.activity.login.WelcomeLoginSignUp;

public class SessionManager {
    // Sharedpref file name
    private static final String PREF_NAME = "planetshare";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public static final String KEY_SELLERID = "sellerId";
    public static final String KEY_VENDORID = "vendorId";
    public static final String KEY_USERID = "userId";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_ACCOUNTID = "accountId";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_EMAIL = "email";

    private static final String IS_LOGIN = "IsSimpleLoggedIn";
    private static final String IS_SOCIAL_LOGIN = "IsSocialIn";


    SharedPreferences pref;
    Editor editor;
    Context context;
    // Shared pref mode;
    int PRIVATE_MODE = 0;


    // Constructor
    public SessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, false);
    }

    /*Token*/
    public void saveToken(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public String getToken() {
        return pref.getString(KEY_TOKEN, null);
    }


    /*AccountId*/
    public void saveAccountId(String accountId) {
        editor.putString(KEY_ACCOUNTID, accountId);
        editor.commit();
    }

    public String getAccountId() {
        return pref.getString(KEY_ACCOUNTID, null);
    }

    /*UserName*/
    public void saveUserName(String name) {
        editor.putString(KEY_USERNAME, name);
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(KEY_USERNAME, null);
    }

    /*EmailId*/
    public void saveEmailId(String email) {
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public String getEmailID() {
        return pref.getString(KEY_EMAIL, null);
    }


    /*UserId*/
    public void saveUserId(int userId, boolean bol) {
        editor.putBoolean(IS_LOGIN, bol);
        editor.putBoolean(IS_SOCIAL_LOGIN, true);
        editor.putInt(KEY_USERID, userId);
        editor.commit();
    }


    public int getUserId() {
        return pref.getInt(KEY_USERID, 0);
    }

    /*VendorId*/
    public void saveVendorId(int vendorId) {
        editor.putInt(KEY_VENDORID, vendorId);
        editor.commit();
    }

    public int getVendorId() {
        return pref.getInt(KEY_VENDORID, 0);
    }

    /*SellerId*/
    public void saveSellerId(int sellerId) {
        editor.putInt(KEY_SELLERID, sellerId);
        editor.commit();
    }

    public int getSellerId() {
        return pref.getInt(KEY_SELLERID, 0);
    }

    /*CheckLogIn*/
    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(context, WelcomeLoginSignUp.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } else {
            Intent i = new Intent(context, DashBoardActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    /*LogOut*/
    public void logoutUser() {
        editor.remove(IS_LOGIN);
        editor.remove(IS_SOCIAL_LOGIN);
        editor.remove(KEY_USERID);
        editor.commit();
        Intent i = new Intent(context, DashBoardActivity.class);
        i.putExtra("has_logged_out", true);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);
    }

}
