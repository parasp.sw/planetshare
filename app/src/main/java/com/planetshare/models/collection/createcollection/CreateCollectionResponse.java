
package com.planetshare.models.collection.createcollection;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class CreateCollectionResponse implements Serializable {
    @SerializedName("collection_id")
    private int collectionId;
    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;

    public int getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(int collectionId) {
        this.collectionId = collectionId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
