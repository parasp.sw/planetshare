
package com.planetshare.models.collection.getusercollection;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class UserCollectionResponse implements Serializable {
    @SerializedName("data")
    private ArrayList<UserData> data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;

    public ArrayList<UserData> getData() {
        return data;
    }

    public void setData(ArrayList<UserData> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
