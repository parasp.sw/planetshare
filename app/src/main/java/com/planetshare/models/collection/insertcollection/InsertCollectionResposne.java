
package com.planetshare.models.collection.insertcollection;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class InsertCollectionResposne implements Serializable {
    @SerializedName("data")
    private ArrayList<Object> data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;

    public ArrayList<Object> getData() {
        return data;
    }

    public void setData(ArrayList<Object> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
