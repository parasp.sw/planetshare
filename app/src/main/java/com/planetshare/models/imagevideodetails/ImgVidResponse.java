
package com.planetshare.models.imagevideodetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class ImgVidResponse {
    @SerializedName("data")
    private ArrayList<ImgVidData> imgVidData;
    @SerializedName("similar_data")
    private ArrayList<SimilarData> similarData;
    @Expose
    private int success;

    public ArrayList<ImgVidData> getData() {
        return imgVidData;
    }

    public void setData(ArrayList<ImgVidData> data) {
        this.imgVidData = data;
    }

    public ArrayList<SimilarData> getSimilarData() {
        return similarData;
    }

    public void setSimilarData(ArrayList<SimilarData> similarData) {
        this.similarData = similarData;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
