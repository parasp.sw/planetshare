
package com.planetshare.models.viewall;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ViewAllRespone implements Serializable {

    @SerializedName("data")
    private ViewAllData data;
    @SerializedName("success")
    private int success;

    public ViewAllData getData() {
        return data;
    }

    public void setData(ViewAllData data) {
        this.data = data;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
