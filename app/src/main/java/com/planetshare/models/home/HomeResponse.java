
package com.planetshare.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class HomeResponse  implements Serializable {

    @SerializedName("image_cat")
    private ArrayList<ImageCat> imageCat;
    @Expose
    private int success;
    @SerializedName("video_cat")
    private ArrayList<VideoCat> videoCat;

    public ArrayList<ImageCat> getImageCat() {
        return imageCat;
    }

    public void setImageCat(ArrayList<ImageCat> imageCat) {
        this.imageCat = imageCat;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ArrayList<VideoCat> getVideoCat() {
        return videoCat;
    }

    public void setVideoCat(ArrayList<VideoCat> videoCat) {
        this.videoCat = videoCat;
    }

}
