package com.planetshare.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.planetshare.activity.R;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class Utils {


    /**
     * Check the current status of the network
     *
     * @param context
     * @return the current network state. true - network is active, false -
     * network is'nt active
     * @throws Exception
     */
    public static NetworkInfo activeNetworkInfo;
    private static String numb;
    private static String uiD;
    private static TelephonyManager tm;
    private static String model, device, deviceID, country, loc, operator;

    public static boolean checkInternetConnection(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {

            activeNetworkInfo = Objects.requireNonNull(cm).getActiveNetworkInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Test for connection
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = Objects.requireNonNull(cm).getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }


    /**
     * Converts a time given in seconds in the format hh:mm:ss
     *
     * @return Time received in the format hh:mm:ss
     */
    public static String durationInSecondsToString(int sec) {
        int hours = sec / 3600;
        int minutes = (sec / 60) - (hours * 60);
        int seconds = sec - (hours * 3600) - (minutes * 60);
        if (hours < 0) {
            hours = 0;
        }
        if (minutes < 0) {
            minutes = 0;
        }
        if (seconds < 0) {
            seconds = 0;
        }
        String formatted = String.format("%d:%02d:%02d", hours, minutes, seconds);

        return formatted;
    }

    /**
     * Rounds the specified value
     *
     * @return Rounded in a given bit rate defined format: if bitrate > 1000 =>
     * bitrate(mb) else bitrate(kb)
     */
    public static StringBuffer roundBitrate(int bitrate) {
        int roundBitrate = Math.round(bitrate / 100) * 100;
        StringBuffer formatted = new StringBuffer();
        if (roundBitrate / 1000 == 0) {
            //Kb
            formatted.append(roundBitrate);
            formatted.append("kb");
        } else {
            //Mb
            formatted.append(roundBitrate / 1000.0);
            formatted.append("mb");
        }
        return formatted;
    }

    public static Map<String, String> userDetails(Context context) {

        Map<String, String> userDetailMap = new HashMap<>();
        try {
            // tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            model = Build.MODEL;
            device = Build.MANUFACTURER;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                numb = Build.getSerial();
            } else {
                numb = Build.SERIAL;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            //FirebaseCrash.report(new Exception("App crashed asking permission and getting build number deviceDetails+Utils line num109"));
        }


      /*  if (numb != null)
            uiD = model.concat(numb);*/


        {
            //can use androidId as qunique too this wil be unique until factoryreset
            //for now nopt using android id
            String android_id = Settings.Secure.ANDROID_ID;
        }
        userDetailMap.put(Constants.KEY_DEVICE, device);
        userDetailMap.put(Constants.KEY_MODEL, model);


        if (tm != null) {
            final String simCountry = tm.getSimCountryIso();
            //   deviceID = tm.getDeviceId();
            userDetailMap.put(Constants.KEY_DEVICEID, numb);
            if (simCountry != null) {

                //      userDetailMap.put(Constants.KEY_COUNTRY, simCountry);
            }
            if (tm != null) {
                //  String locationarea = String.valueOf(cellLocation.getLac());
                userDetailMap.put(Constants.KEY_OPERATOR, tm.getNetworkOperatorName());
                //    userDetailMap.put(Constants.KEY_LOCATION, locationarea);
//                Log.e("kanish", "in UTILS:User details:sim operator name" + tm.getSimOperator()
//                        + "cell Location" + "lon" +
//                        "network operator name" + tm.getNetworkOperatorName() + "device id" + tm.getDeviceId());
            } else {

            }


        }
        return userDetailMap;
    }

    public static void toastIt(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT);
    }

    public static void restartDialogue(Context context, String message, final boolean isExit) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        if (isExit)
            alertDialogBuilder.setCancelable(false);
        else
            alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (isExit)
                            System.exit(0);
                        else
                            arg0.dismiss();
                        // context.startActivity(new Intent(context, DownloadActivity.class));
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
            }
        });
        alertDialog.show();
    }
/*
    public static void restartDialogue(Context context, String message, final boolean isExit) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //System.exit(0);
                        context.startActivity(new Intent(context, DownloadActivity.class));
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
*/


    public static void nodata(Context context, String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
            }
        });
        alertDialog.show();
    }

    public static String isTablet(Context context) {
        boolean isTab600 = true;
        boolean isTab720 = true;
        boolean isPhone = true;
        try {
            //context.getResources().getBoolean(R.bool.isTablet720);
        } catch (RuntimeException rp) {

            isTab720 = false;
            try {
                // context.getResources().getBoolean(R.bool.isTablet600);
            } catch (RuntimeException rp1) {
                isTab600 = false;
                try {
                    context.getResources().getBoolean(R.bool.isphone);
                } catch (RuntimeException rp2) {
                    isPhone = false;
                }
            }


        }

        if (isTab600) {
//            getConstants(Constants.IS_TABLET_600);

            return Constants.IS_TABLET_600;
        } else if (isTab720) {
            //getConstants(Constants.IS_TABLET_720);

            return Constants.IS_TABLET_720;
        } else if (isPhone) {
//            getConstants(Constants.IS_PHONE);

            return Constants.IS_PHONE;
        }
        return null;
    }

    public static String getConstants(String value) {
        return value;
    }

    public String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();


            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

 }
