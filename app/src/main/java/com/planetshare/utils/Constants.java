package com.planetshare.utils;

public class Constants {

    public static final String UserNameAuth = "PlanetCastM";
    public static final String PasswordAuth = "p1@netc@$t";
    public static final String KEYIMAGELIST = "imagelist";
    public static final String KEYVIDEOLIST = "videolist";
    public static final String TAG = "mobile";
    // Global variable used to store network state
    public static boolean isNetworkConnected = false;
    public static final String KEY_MODEL = "KEY_MODEL";
    public static final String KEY_OPERATOR = "KEY_OPERATOR";
    public static final String KEY_DEVICEID = "KEY_DEVICEID";
    public static final String KEY_DEVICE = "KEY_DEVICE";
    public static final String NETWORK_ERROR = "No Internet Connection Available";
    public static final String NETWORK_FAILURE = "Network failure Please Try Again";
    public static final String SOMETHING_WENT_WRONG = "Something Went Wrong";
    public static final String IS_TABLET_600 = "TABLET600";
    public static final String IS_TABLET_720 = "TABLET600";
    public static final String IS_PHONE = "PHONE";

}
